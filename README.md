# goserialtap

[![pipeline status](https://gitlab.com/osechet/goserialtap/badges/master/pipeline.svg)](https://gitlab.com/osechet/goserialtap/commits/master)
[![coverage report](https://gitlab.com/osechet/goserialtap/badges/master/coverage.svg)](https://gitlab.com/osechet/goserialtap/commits/master)

goserialtap is a TAP implementation that redirects data from a TAP interface to a serial port. It can be used to make TCP/IP applications communicates over a serial line. Ethernet frames are encapsulated using the SLIP protocol.

## Install

```sh
go install gitlab.com/osechet/goserialtap@latest
```

## Usage

```sh
goserialtap -help
  -baudrate int
      the serial speed (default 115200)
  -help
      show this message
  -interface string
      the tap interface to use (default "tap0")
  -owner string
      the owner of the tap interface (default current user)
  -serial string
      the serial device to use (default "/dev/ttyS0")
```

Example:

```sh
goserialtap -interface tap5 -serial /dev/ttyS1 -baudrate 9600
```

## TAP

### Linux TAP interface

To create a TAP interface, use this command, replacing the IP address:

```sh
nmcli connection add type tun ifname tap0 con-name "TAP" mode tap owner `id -u` ip4 192.168.81.254/24
```

If the owner of the tap interface is not the current user, you need to specify it using the `-owner` argument:

```sh
goserialtap -owner otherUser
```

### Windows Tap Interface

Not tested.

You need to install a [tap driver](https://github.com/OpenVPN/tap-windows6). Then call `goserialtap` with the `-i` argument to specify the `tap` interface.

## Serial

### Linux Serial Device

To be able to open the serial device as a non-root user, add the user to the `dialout` group:

```sh
sudo usermod -a -G dialout $USER
```

## Building

goserialtap is built with [goreleaser](https://goreleaser.com/). To build locally:

```sh
goreleaser build --clean --snapshot
```

## Testing

Unit tests and BDD tests can be ran with `go test`:

```sh
go test ./...
```

Coverage reports can be generated with `task coverage`.
