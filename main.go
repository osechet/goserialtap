package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/songgao/water"
	"github.com/tarm/serial"
	"gitlab.com/osechet/goserialtap/serialtap"
)

const defaultInterface = "tap0"
const defaultSerial = "/dev/ttyS0"
const defaultBaudrate = 115200

var (
	version   = "dev"
	commit    = "none"
	treeState = ""
	date      = "unknown"
)

var usage bool
var showVersion bool
var debug bool
var iface string
var serialDevice string
var baudrate int
var owner string

type tapOpener func(config water.Config) (io.ReadWriteCloser, error)
type serialOpener func(config serial.Config) (io.ReadWriteCloser, error)

func initLogging() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
}

func setupTap(createTapFunc tapOpener) (tap io.ReadWriteCloser, err error) {
	config := water.Config{
		DeviceType: water.TAP,
	}
	updateTapConfig(&config)

	tap, err = createTapFunc(config)
	if err != nil {
		return nil, err
	}
	return tap, nil
}

func setupSerial(createSerialFunc serialOpener) (serialPort io.ReadWriteCloser, err error) {
	config := serial.Config{Name: serialDevice, Baud: baudrate}

	log.Info().Msgf("Serial  : %s", serialDevice)
	log.Info().Msgf("Baudrate: %d", baudrate)

	serialPort, err = createSerialFunc(config)
	if err != nil {
		return nil, err
	}
	return serialPort, nil
}

func initFlags() {
	flag.BoolVar(&usage, "help", false, "show this message")
	flag.BoolVar(&showVersion, "version", false, "display the version")
	flag.BoolVar(&debug, "debug", false, "print the debug logs")
	flag.StringVar(&iface, "interface", defaultInterface, "the tap interface to use")
	flag.StringVar(&serialDevice, "serial", defaultSerial, "the serial device to use")
	flag.IntVar(&baudrate, "baudrate", defaultBaudrate, "the serial speed")
	flag.StringVar(&owner, "owner", "", "the owner of the tap interface (default current user)")
}

func printUsage() {
	fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
}

func getVersion() string {
	if treeState == "true" {
		treeState = "+"
	} else {
		treeState = ""
	}
	return fmt.Sprintf("%s (%s%s) %s", version, commit, treeState, date)
}

func run() int {
	initFlags()
	flag.Parse()

	if usage {
		printUsage()
		return 0
	}
	if showVersion {
		fmt.Printf("%s version %s\n", filepath.Base(os.Args[0]), getVersion())
		return 0
	}

	initLogging()

	log.Info().Msg("Application started")
	log.Info().Msg("========================================")

	tap, err := setupTap(func(config water.Config) (io.ReadWriteCloser, error) {
		return water.New(config)
	})
	if err != nil {
		log.Error().Err(err).Msg("An error occured when configuring the tap interface.")
		return 1
	}
	defer tap.Close()

	serialPort, err := setupSerial(func(config serial.Config) (io.ReadWriteCloser, error) {
		return serial.OpenPort(&config)
	})
	if err != nil {
		log.Error().Err(err).Msg("An error occured when configuring the serial interface.")
		return 2
	}
	defer serialPort.Close()

	err = serialtap.Attach(tap, serialPort)
	if err != nil {
		return 1
	}
	return 0
}

func main() {
	os.Exit(run())
}
