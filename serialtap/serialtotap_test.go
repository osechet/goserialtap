package serialtap

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"reflect"
	"time"

	"github.com/lobaro/slip"
)

type serialtapFeature struct {
	internalTap    *GenericInterface
	externalTap    *GenericInterface
	internalSerial *GenericInterface
	externalSerial *GenericInterface

	sentFrame     []byte
	receivedFrame []byte
	exitError     error
}

func (f *serialtapFeature) anOpenedTAPInterface() error {
	pr1, pw1 := io.Pipe()
	pr2, pw2 := io.Pipe()

	f.internalTap = NewGenericInterface(pr1, pw2)
	f.externalTap = NewGenericInterface(pr2, pw1)

	f.internalTap.RefuseData = false
	f.externalTap.RefuseData = false
	return nil
}

func (f *serialtapFeature) anOpenedSerialInterfaces() error {
	pr1, pw1 := io.Pipe()
	pr2, pw2 := io.Pipe()

	f.internalSerial = NewGenericInterface(pr1, pw2)
	f.externalSerial = NewGenericInterface(pr2, pw1)

	f.internalTap.RefuseData = false
	f.externalTap.RefuseData = false
	return nil
}

func (f *serialtapFeature) anAttachedProgram() error {
	go func() {
		f.exitError = Attach(f.internalTap, f.internalSerial)
	}()
	return nil
}

func (f *serialtapFeature) aFrameIsWrittenOnTheTAPInterface() error {
	f.sentFrame = []byte{0x1, 0x2, 0x3, 0x4}
	_, err := f.externalTap.Write(f.sentFrame)
	if err != nil {
		return errors.New("failed to write frame")
	}
	return nil
}

func (f *serialtapFeature) theFrameIsEncapsulatedWithSLIPAndRedirectedToTheSerialInterface() error {
	var err error
	reader := slip.NewReader(bufio.NewReader(f.externalSerial))
	f.receivedFrame, _, err = reader.ReadPacket()
	if err != nil {
		return fmt.Errorf("cannot read serial interface: %v", err)
	}
	if !reflect.DeepEqual(f.sentFrame, f.receivedFrame) {
		return fmt.Errorf("expected %v, got %v", f.sentFrame, f.receivedFrame)
	}

	return nil
}

func (f *serialtapFeature) aValidSLIPFrameIsReceivedFromTheSerial() error {
	f.sentFrame = []byte{0x1, 0x2, 0x3, 0x4, slip.END}
	_, err := f.externalSerial.Write(f.sentFrame)
	if err != nil {
		return errors.New("failed to write frame")
	}
	return nil
}

func (f *serialtapFeature) anInvalidFrameIsReceivedFromTheSerial() error {
	f.sentFrame = []byte{0x1, 0x2, 0x3, 0x4}
	_, err := f.externalSerial.Write(f.sentFrame)
	if err != nil {
		return errors.New("failed to write frame")
	}
	return nil
}

func (f *serialtapFeature) theFrameIsDecapsulatedFromSLIPAndRedirectedToTheTAPInterface() error {
	f.receivedFrame = make([]byte, 1500)
	n, err := f.externalTap.Read(f.receivedFrame)
	if err != nil {
		return fmt.Errorf("cannot read TAP interface: %v", err)
	}
	f.receivedFrame = f.receivedFrame[:n]
	expected := f.sentFrame[:len(f.sentFrame)-1]
	if !reflect.DeepEqual(expected, f.receivedFrame) {
		return fmt.Errorf("expected %v, got %v", expected, f.receivedFrame)
	}

	return nil
}

func (f *serialtapFeature) aPartialFrameIsReceivedFromTheSerial() error {
	f.sentFrame = []byte{0x1, 0x2, 0x3, 0x4}
	_, err := f.externalSerial.Write(f.sentFrame)
	if err != nil {
		return errors.New("failed to write frame")
	}
	return nil
}

func (f *serialtapFeature) theFrameIsIgnored() error {
	// Just check if data is available
	reader := bufio.NewReader(f.externalTap)
	if reader.Buffered() > 0 {
		return fmt.Errorf("data received, none expected")
	}
	return nil
}

func (f *serialtapFeature) anErrorOccuredWhenWritingOnTheTapInterface() error {
	f.internalTap.RefuseData = true
	f.sentFrame = []byte{0x1, 0x2, 0x3, 0x4, slip.END}
	_, err := f.externalSerial.Write(f.sentFrame)
	if err != nil {
		return errors.New("failed to write frame")
	}
	return nil
}

func (f *serialtapFeature) theApplicationExitsWithError() error {
	time.Sleep(250 * time.Millisecond)
	if f.exitError == nil {
		return fmt.Errorf("the application did not exit with error")
	}
	return nil
}
