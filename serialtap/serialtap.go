package serialtap

import (
	"io"

	"github.com/lobaro/slip"
	"github.com/rs/zerolog/log"
	"github.com/songgao/packets/ethernet"
)

func readAndDecode(reader *slip.Reader) ([]byte, error) {
	frame, _, err := reader.ReadPacket()
	if err != nil {
		return frame, err
	}
	return frame, nil
}

func encodeAndWrite(writer *slip.Writer, frame []byte) error {
	err := writer.WritePacket(frame)
	if err != nil {
		return err
	}
	return nil
}

type tapMgr struct {
	tap    io.Reader
	serial io.Writer

	quit chan bool
}

func newTapMgr(tap io.Reader, serial io.Writer) *tapMgr {
	return &tapMgr{tap, serial, make(chan bool)}
}

func (mgr *tapMgr) Start() error {
	err := mgr.Run()
	if err != nil {
		log.Error().Msgf("An error occurred with tap manager: %v", err)
		return err
	}
	return nil
}

func (mgr *tapMgr) Stop() {
	mgr.quit <- true
	log.Debug().Msg("tap stopped")
}

func (mgr *tapMgr) Run() error {
	var frame ethernet.Frame

	writer := slip.NewWriter(mgr.serial)

	for {
		select {
		default:
			frame.Resize(1500)
			log.Debug().Msg("tap waiting for data")
			n, err := mgr.tap.Read(frame)
			if err != nil {
				// If the write fails, there is a high chance the tap link is down. We don't want to continue forever.
				log.Error().Msgf("tap: An error occurred when reading the tap interface: %v. Frame: %v", err, frame)
				return err
			}
			frame = frame[:n]
			log.Debug().Msgf("tap received frame %d", len(frame))

			err = encodeAndWrite(writer, frame)
			if err != nil {
				// If we cannot encode the frame, ignore it and continue.
				log.Error().Msgf("tap: An error occurred when encoding SLIP frame: %v. Frame: %v", err, frame)
				continue
			}
			log.Debug().Msgf("frame sent on serial %d", len(frame))
		case <-mgr.quit:
			// stop
			log.Debug().Msg("Stopping tap mgr")
			return nil
		}
	}
}

type serialMgr struct {
	serial io.Reader
	tap    io.Writer

	quit chan bool
}

func newSerialMgr(serial io.Reader, tap io.Writer) *serialMgr {
	return &serialMgr{serial, tap, make(chan bool)}
}

func (mgr *serialMgr) Start() error {
	err := mgr.Run()
	if err != nil {
		log.Error().Msgf("An error occurred with serial manager: %v", err)
		return err
	}
	return nil
}

func (mgr *serialMgr) Stop() {
	mgr.quit <- true
	log.Debug().Msg("serial stopped")
}

func (mgr *serialMgr) Run() error {
	reader := slip.NewReader(mgr.serial)

	for {
		select {
		default:
			log.Debug().Msg("serial waiting for data")
			frame, err := readAndDecode(reader)
			if err != nil {
				// Only invalid SLIP frames leads to decoding errors, just ignore the frame and continue.
				log.Error().Msgf("serial: An error occurred when decoding SLIP frame: %v. Frame: %v", err, frame)
				continue
			}
			log.Debug().Msgf("serial received frame %d", len(frame))

			n, err := mgr.tap.Write(frame)
			if err != nil {
				// If the write fails, there is a high chance the serial link is down. We don't want to continue forever.
				log.Error().Msgf("serial: An error occurred when writing to the tap interface: %v. Frame: %v", err, frame)
				return err
			}
			log.Debug().Msgf("frame sent on tap %d", n)
		case <-mgr.quit:
			// stop
			log.Debug().Msg("Stopping serial mgr")
			return nil
		}
	}
}

// Attach starts the routines that listens on tap and serial and redirect data.
func Attach(tap io.ReadWriteCloser, serial io.ReadWriteCloser) error {
	// Don't use a WaitGroup here since the application must exit as soon as 1 routine exits.
	finished := make(chan error)

	tap2Serial := newTapMgr(tap, serial)
	serial2Tap := newSerialMgr(serial, tap)

	go func() {
		finished <- tap2Serial.Start()
	}()
	go func() {
		finished <- serial2Tap.Start()
	}()

	status := <-finished

	// TODO: find a way to make the Stop() functions to work, maybe using Close().
	// For the moment, if the routine is blocked on Read(), the Stop() blocks.
	// tap2Serial.Stop()
	// serial2Tap.Stop()

	// status contains the status of the first thread to exit (the one that failed)
	return status
}
