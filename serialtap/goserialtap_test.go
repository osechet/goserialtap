package serialtap

import (
	"errors"
	"fmt"
	"io"
	"net"
	"reflect"

	"github.com/cucumber/godog"
	"github.com/rs/zerolog"
	"github.com/songgao/packets/ethernet"
)

type goserialtapFeature struct {
	tap1    *GenericInterface
	tap2    *GenericInterface
	serial1 *GenericInterface
	serial2 *GenericInterface

	sentFrame     ethernet.Frame
	receivedFrame ethernet.Frame
}

func (f *goserialtapFeature) twoOpenedTAPInterfaces() error {
	f.tap1 = NewGenericInterface(io.Pipe())
	f.tap2 = NewGenericInterface(io.Pipe())
	return nil
}

func (f *goserialtapFeature) twoOpenedSerialInterfacesLinkedToEachOthers() error {
	pr1, pw1 := io.Pipe()
	pr2, pw2 := io.Pipe()

	f.serial1 = NewGenericInterface(pr1, pw2)
	f.serial2 = NewGenericInterface(pr2, pw1)

	return nil
}

func (f *goserialtapFeature) twoAttachedPrograms() error {
	go func() {
		err := Attach(f.tap1, f.serial1)
		if err != nil {
			// fail the test
			panic(err)
		}
	}()
	go func() {
		err := Attach(f.tap2, f.serial2)
		if err != nil {
			// fail the test
			panic(err)
		}
	}()
	return nil
}

func (f *goserialtapFeature) aFrameIsWrittenOnTheFirstTAPInterface() error {
	dst, _ := net.ParseMAC("01:23:45:67:89:01")
	src, _ := net.ParseMAC("23:45:67:89:01:23")
	f.sentFrame.Prepare(dst, src, ethernet.Tagged, ethernet.IPv4, 80)
	_, err := f.tap1.Write(f.sentFrame)
	if err != nil {
		return errors.New("failed to write frame")
	}
	return nil
}

func (f *goserialtapFeature) theFrameIsReceivedOnTheSecondTAPInterface() error {
	f.receivedFrame.Resize(1500)
	n, err := f.tap2.Read(f.receivedFrame)
	if err != nil {
		return fmt.Errorf("cannot read TAP interface: %v", err)
	}
	f.receivedFrame = f.receivedFrame[:n]
	if !reflect.DeepEqual(f.sentFrame, f.receivedFrame) {
		return fmt.Errorf("expected %d bytes to read, got %d", len(f.sentFrame), len(f.receivedFrame))
	}

	return nil
}

//

func BeforeFeature(s *godog.Suite) {
	zerolog.SetGlobalLevel(zerolog.Disabled)
}

func FeatureContext(s *godog.Suite) {
	feat := &serialtapFeature{}

	s.Step(`^an opened TAP interface$`, feat.anOpenedTAPInterface)
	s.Step(`^an opened serial interfaces$`, feat.anOpenedSerialInterfaces)
	s.Step(`^an attached program$`, feat.anAttachedProgram)
	s.Step(`^a valid SLIP frame is received from the serial$`, feat.aValidSLIPFrameIsReceivedFromTheSerial)
	s.Step(`^the frame is decapsulated from SLIP and redirected to the TAP interface$`, feat.theFrameIsDecapsulatedFromSLIPAndRedirectedToTheTAPInterface)
	s.Step(`^a partial frame is received from the serial$`, feat.aPartialFrameIsReceivedFromTheSerial)
	s.Step(`^an invalid frame is received from the serial$`, feat.anInvalidFrameIsReceivedFromTheSerial)
	s.Step(`^the frame is ignored$`, feat.theFrameIsIgnored)
	s.Step(`^a frame is written on the TAP interface$`, feat.aFrameIsWrittenOnTheTAPInterface)
	s.Step(`^the frame is encapsulated with SLIP and redirected to the serial interface$`, feat.theFrameIsEncapsulatedWithSLIPAndRedirectedToTheSerialInterface)
	s.Step(`^an error occured when writing on the tap interface$`, feat.anErrorOccuredWhenWritingOnTheTapInterface)
	s.Step(`^the application exits with error$`, feat.theApplicationExitsWithError)

	e2efeat := &goserialtapFeature{}

	s.Step(`^two opened TAP interfaces$`, e2efeat.twoOpenedTAPInterfaces)
	s.Step(`^two opened serial interfaces linked to each others$`, e2efeat.twoOpenedSerialInterfacesLinkedToEachOthers)
	s.Step(`^two attached programs$`, e2efeat.twoAttachedPrograms)
	s.Step(`^a frame is written on the first TAP interface$`, e2efeat.aFrameIsWrittenOnTheFirstTAPInterface)
	s.Step(`^the frame is received on the second TAP interface$`, e2efeat.theFrameIsReceivedOnTheSecondTAPInterface)
}
