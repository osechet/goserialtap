package serialtap

import (
	"bytes"
	"errors"
	"io"
	"reflect"
	"testing"
	"time"

	"github.com/lobaro/slip"
)

func Test_readAndDecode(t *testing.T) {
	type args struct {
		reader *slip.Reader
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{"Valid SLIP frame", args{slip.NewReader(bytes.NewReader([]byte{0x1, 0x2, 0x3, 0x4, slip.END}))}, []byte{0x1, 0x2, 0x3, 0x4}, false},
		{"Valid SLIP frame with prefix", args{slip.NewReader(bytes.NewReader([]byte{slip.END, 0x1, 0x2, 0x3, 0x4, slip.END}))}, []byte{0x1, 0x2, 0x3, 0x4}, false},
		{"Valid SLIP frame with END", args{slip.NewReader(bytes.NewReader([]byte{0x1, 0x2, slip.ESC, slip.ESC_END, 0x4, slip.END}))}, []byte{0x1, 0x2, slip.END, 0x4}, false},
		{"Valid SLIP frame with ESC", args{slip.NewReader(bytes.NewReader([]byte{0x1, 0x2, slip.ESC, slip.ESC_ESC, 0x4, slip.END}))}, []byte{0x1, 0x2, slip.ESC, 0x4}, false},
		{"Invalid SLIP frame", args{slip.NewReader(bytes.NewReader([]byte{0x1, 0x2, 0x3, 0x4}))}, []byte{0x1, 0x2, 0x3, 0x4}, true},
		{"2 SLIP frames", args{slip.NewReader(bytes.NewReader([]byte{0x1, 0x2, slip.END, 0x3, 0x4, slip.END}))}, []byte{0x1, 0x2}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := readAndDecode(tt.args.reader)
			if (err != nil) != tt.wantErr {
				t.Errorf("readAndDecode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readAndDecode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_readAndDecodeConsecutive(t *testing.T) {
	type args struct {
		frames [][]byte
	}
	tests := []struct {
		name    string
		args    args
		want    [][]byte
		wantErr []bool
	}{
		{"Valid SLIP frames", args{[][]byte{{0x1, 0x2, slip.END}, {0x3, 0x4, slip.END}}}, [][]byte{{0x1, 0x2}, {0x3, 0x4}}, []bool{false, false}},
		{"Valid SLIP frames with prefix", args{[][]byte{{slip.END, 0x1, 0x2, slip.END}, {slip.END, 0x3, 0x4, slip.END}}}, [][]byte{{0x1, 0x2}, {0x3, 0x4}}, []bool{false, false}},
		{"Valid SLIP frame with END", args{[][]byte{{slip.END, 0x1, slip.ESC, slip.ESC_END, 0x2, slip.END}, {slip.END, 0x3, slip.ESC, slip.ESC_END, 0x4, slip.END}}}, [][]byte{{0x1, slip.END, 0x2}, {0x3, slip.END, 0x4}}, []bool{false, false}},
		{"Valid SLIP frame with ESC", args{[][]byte{{slip.END, 0x1, slip.ESC, slip.ESC_ESC, 0x2, slip.END}, {slip.END, 0x3, slip.ESC, slip.ESC_ESC, 0x4, slip.END}}}, [][]byte{{0x1, slip.ESC, 0x2}, {0x3, slip.ESC, 0x4}}, []bool{false, false}},
		{"Invalid SLIP frames", args{[][]byte{{0x1, 0x2}, {0x3, 0x4}}}, [][]byte{{0x1, 0x2, 0x3, 0x4}, nil}, []bool{true, true}},
		{"Invalid SLIP frame then valid", args{[][]byte{{0x1, 0x2}, {0x3, 0x4, slip.END}}}, [][]byte{{0x1, 0x2, 0x3, 0x4}, nil}, []bool{false, true}},
		{"Valid SLIP frame then invalid", args{[][]byte{{0x1, 0x2, slip.END}, {0x3, 0x4}}}, [][]byte{{0x1, 0x2}, {0x3, 0x4}}, []bool{false, true}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			data := make([]byte, 0)
			for _, frame := range tt.args.frames {
				data = append(data, frame...)
			}
			reader := slip.NewReader(bytes.NewReader(data))
			for i := range tt.args.frames {
				got, err := readAndDecode(reader)
				if (err != nil) != tt.wantErr[i] {
					t.Errorf("readAndDecode() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
				if !reflect.DeepEqual(got, tt.want[i]) {
					t.Errorf("readAndDecode() = %v, want %v", got, tt.want[i])
				}
			}
		})
	}
}

func Test_encodeAndWrite(t *testing.T) {
	type args struct {
		buffer *bytes.Buffer
		frame  []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{"Simple frame", args{bytes.NewBuffer([]byte{}), []byte{0x1, 0x2, 0x3, 0x4}}, []byte{slip.END, 0x1, 0x2, 0x3, 0x4, slip.END}, false},
		{"Frame with END", args{bytes.NewBuffer([]byte{}), []byte{0x1, 0x2, slip.END, 0x4}}, []byte{slip.END, 0x1, 0x2, slip.ESC, slip.ESC_END, 0x4, slip.END}, false},
		{"Frame with ESC", args{bytes.NewBuffer([]byte{}), []byte{0x1, 0x2, slip.ESC, 0x4}}, []byte{slip.END, 0x1, 0x2, slip.ESC, slip.ESC_ESC, 0x4, slip.END}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := slip.NewWriter(tt.args.buffer)
			if err := encodeAndWrite(writer, tt.args.frame); (err != nil) != tt.wantErr {
				t.Errorf("encodeAndWrite() error = %v, wantErr %v", err, tt.wantErr)
			}
			got := tt.args.buffer.Bytes()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("encodeAndWrite() = %v, want %v", got, tt.want)
			}
		})
	}
}

var result []byte

func benchmarkReadAndDecode(i int, b *testing.B) {
	var frame []byte
	data := make([]byte, i)
	data = append(data, slip.END)
	reader := slip.NewReader(bytes.NewBuffer(data))
	for n := 0; n < b.N; n++ {
		frame, _ = readAndDecode(reader)
	}
	// store the result to a package level variable
	// so the compiler cannot eliminate the Benchmark itself.
	result = frame
}

func BenchmarkReadAndDecode1(b *testing.B)     { benchmarkReadAndDecode(1, b) }
func BenchmarkReadAndDecode10(b *testing.B)    { benchmarkReadAndDecode(10, b) }
func BenchmarkReadAndDecode100(b *testing.B)   { benchmarkReadAndDecode(100, b) }
func BenchmarkReadAndDecode1000(b *testing.B)  { benchmarkReadAndDecode(1000, b) }
func BenchmarkReadAndDecode2000(b *testing.B)  { benchmarkReadAndDecode(2000, b) }
func BenchmarkReadAndDecode4000(b *testing.B)  { benchmarkReadAndDecode(4000, b) }
func BenchmarkReadAndDecode10000(b *testing.B) { benchmarkReadAndDecode(10000, b) }

type ErrorReader struct {
}

func (r ErrorReader) Read(buf []byte) (int, error) {
	return 0, io.EOF
}

func (r ErrorReader) Close() error {
	return nil
}

type DelayReader struct {
	delay time.Duration
}

func (r DelayReader) Read(buf []byte) (int, error) {
	time.Sleep(r.delay * time.Second)
	return 0, nil
}

func (r DelayReader) Close() error {
	return nil
}

type ErrorWriter struct {
}

func (r ErrorWriter) Write(p []byte) (int, error) {
	return 0, errors.New("An error")
}

func Test_tapMgr_Run(t *testing.T) {
	type fields struct {
		tap    io.Reader
		serial io.Writer
		quit   chan bool
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
		err     error
	}{
		{"Reading error", fields{ErrorReader{}, bytes.NewBuffer(make([]byte, 0)), make(chan bool)}, true, io.EOF},
		// When write fails, we continue to read. That leads to EOF.
		{"Writing error", fields{bytes.NewBuffer([]byte{0x1, 0x2, 0x3}), ErrorWriter{}, make(chan bool)}, true, io.EOF},
		{"Stop loop", fields{DelayReader{2}, bytes.NewBuffer(make([]byte, 0)), make(chan bool)}, false, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mgr := &tapMgr{
				tap:    tt.fields.tap,
				serial: tt.fields.serial,
				quit:   tt.fields.quit,
			}
			timer := time.AfterFunc(3*time.Second, func() {
				mgr.Stop()
			})
			err := mgr.Run()
			if (err != nil) != tt.wantErr {
				t.Errorf("tapMgr.Run() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.err != nil && err != tt.err {
				t.Errorf("tapMgr.Run() error = %v, want %v", err, tt.err)
			}
			timer.Stop()
		})
	}
}

func Test_serialMgr_Run(t *testing.T) {
	type fields struct {
		serial io.Reader
		tap    io.Writer
		quit   chan bool
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{"Reading error", fields{ErrorReader{}, bytes.NewBuffer(make([]byte, 0)), make(chan bool)}, false},
		{"Writing error", fields{bytes.NewBuffer([]byte{0x1, 0x2, 0x3}), ErrorWriter{}, make(chan bool)}, false},
		{"Stop loop", fields{DelayReader{2}, bytes.NewBuffer(make([]byte, 0)), make(chan bool)}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mgr := &serialMgr{
				serial: tt.fields.serial,
				tap:    tt.fields.tap,
				quit:   tt.fields.quit,
			}
			timer := time.AfterFunc(3*time.Second, func() {
				mgr.Stop()
			})
			if err := mgr.Run(); (err != nil) != tt.wantErr {
				t.Errorf("serialMgr.Run() error = %v, wantErr %v", err, tt.wantErr)
			}
			timer.Stop()
		})
	}
}
