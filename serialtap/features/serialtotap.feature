Feature: Redirect data from serial to TAP
  In order to have TCP/IP communication over serial
  As a user
  I want data received from a serial interface to be redirected to a TAP interface

  Scenario: Frames received from serial are redirected to a TAP interface
    Given an opened TAP interface
    And an opened serial interfaces
    And an attached program
    When a valid SLIP frame is received from the serial
    Then the frame is decapsulated from SLIP and redirected to the TAP interface

  Scenario: Partial frames are ignored
    Given an opened TAP interface
    And an opened serial interfaces
    And an attached program
    When a partial frame is received from the serial
    Then the frame is ignored

  Scenario: Invalid frames are ignored
    Given an opened TAP interface
    And an opened serial interfaces
    And an attached program
    When an invalid frame is received from the serial
    Then the frame is ignored

  Scenario: Application exits on IO error
    Given an opened TAP interface
    And an opened serial interfaces
    And an attached program
    When an error occured when writing on the tap interface
    Then the application exits with error
