Feature: Transfer TCP/IP data over serial
  In order to have TCP/IP communication over serial
  As a user
  I want data sent on a TAP interface to be received on another TAP interface

  Scenario: Frames sent on a TAP are transfered to another TAP
    Given two opened TAP interfaces
    And two opened serial interfaces linked to each others
    And two attached programs
    When a frame is written on the first TAP interface
    Then the frame is received on the second TAP interface
