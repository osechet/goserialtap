Feature: Redirect data from TAP to serial
  In order to have TCP/IP communication over serial
  As a user
  I want data sent to a TAP interface to be redirected to a serial interface

  Scenario: Frames sent to a TAP are redirected to a serial interface
    Given an opened TAP interface
    And an opened serial interfaces
    And an attached program
    When a frame is written on the TAP interface
    Then the frame is encapsulated with SLIP and redirected to the serial interface
