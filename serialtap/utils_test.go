package serialtap

import (
	"fmt"
	"io"
)

// GenericInterface is defined for tests purpose only. GenericInterface is a io.ReadWriteCloser.
type GenericInterface struct {
	reader io.Reader
	writer io.Writer

	RefuseData bool
}

func NewGenericInterface(reader io.Reader, writer io.Writer) *GenericInterface {
	return &GenericInterface{reader, writer, false}
}

func (i GenericInterface) Read(p []byte) (n int, err error) {
	return i.reader.Read(p)
}

func (i *GenericInterface) Write(p []byte) (n int, err error) {
	if i.RefuseData {
		return -1, fmt.Errorf("An error occured when writing")
	}
	return i.writer.Write(p)
}

func (i *GenericInterface) Close() error {
	return nil
}
