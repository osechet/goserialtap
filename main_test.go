package main

import (
	"bytes"
	"flag"
	"io"
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/songgao/water"
	"github.com/stretchr/testify/require"
	"github.com/tarm/serial"
)

type GenericInterface struct {
}

// NewGenericInterface creates a new GenericInterface.
func NewGenericInterface() *GenericInterface {
	return &GenericInterface{}
}

func (i GenericInterface) Read(p []byte) (n int, err error) {
	return 0, nil
}

func (i *GenericInterface) Write(p []byte) (n int, err error) {
	return 0, nil
}

func (i *GenericInterface) Close() error {
	return nil
}

func Test_setupTap(t *testing.T) {
	type args struct {
		tapOpener tapOpener
	}
	tests := []struct {
		name    string
		args    args
		wantTap bool
		wantErr bool
	}{
		{"Invalid tap", args{func(config water.Config) (io.ReadWriteCloser, error) { return nil, io.EOF }}, false, true},
		{"Valid", args{func(config water.Config) (io.ReadWriteCloser, error) { return NewGenericInterface(), nil }}, true, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTap, err := setupTap(tt.args.tapOpener)
			if (gotTap != nil) != tt.wantTap {
				t.Errorf("run() tap = %v, wantTap %v", gotTap, tt.wantTap)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("run() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_setupSerial(t *testing.T) {
	type args struct {
		serialOpener serialOpener
	}
	tests := []struct {
		name           string
		args           args
		wantSerialPort bool
		wantErr        bool
	}{
		{"Invalid serial", args{func(config serial.Config) (io.ReadWriteCloser, error) { return nil, io.EOF }}, false, true},
		{"Valid", args{func(config serial.Config) (io.ReadWriteCloser, error) { return NewGenericInterface(), nil }}, true, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotSerial, err := setupSerial(tt.args.serialOpener)
			if (gotSerial != nil) != tt.wantSerialPort {
				t.Errorf("run() serialPort = %v, gotSerial %v", gotSerial, tt.wantSerialPort)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("run() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_initLogging(t *testing.T) {
	tests := []struct {
		name  string
		debug bool
		want  zerolog.Level
	}{
		{"Default log level", false, zerolog.InfoLevel},
		{"Debug log level", true, zerolog.DebugLevel},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			debug = tt.debug
			initLogging()
			level := zerolog.GlobalLevel()
			if level != tt.want {
				t.Errorf("log level = %v, want %v", level, tt.want)
			}
		})
	}
}

func Test_getOwner(t *testing.T) {
	type args struct {
		owner string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"No owner", args{""}, false},
		{"Current user", args{os.Getenv("USERNAME")}, false},
		{"Root", args{"root"}, false},
		{"Other user", args{"notauser"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, _, err := getOwner(tt.args.owner)
			if (err != nil) != tt.wantErr {
				t.Errorf("getOwner() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_run(t *testing.T) {
	tests := []struct {
		name    string
		args    []string
		wantOut string
		wantErr string
		want    int
	}{
		{"help", []string{"-help"}, "", `Usage of goserialtap:
  -baudrate int
    	the serial speed (default 115200)
  -debug
    	print the debug logs
  -help
    	show this message
  -interface string
    	the tap interface to use (default "tap0")
  -owner string
    	the owner of the tap interface (default current user)
  -serial string
    	the serial device to use (default "/dev/ttyS0")
  -version
    	display the version
`, 0},
		{"version", []string{"-version"}, "goserialtap version dev (none) unknown\n", "", 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ResetFlagForTesting(nil)
			os.Args = append([]string{"goserialtap"}, tt.args...)

			oldOut := os.Stdout
			oldErr := os.Stderr
			or, ow, _ := os.Pipe()
			er, ew, _ := os.Pipe()
			os.Stdout = ow
			os.Stderr = ew

			outC := make(chan string)
			errC := make(chan string)
			go func() {
				var buf bytes.Buffer
				_, err := io.Copy(&buf, or)
				require.NoError(t, err)
				outC <- buf.String()
			}()
			go func() {
				var buf bytes.Buffer
				_, err := io.Copy(&buf, er)
				require.NoError(t, err)
				errC <- buf.String()
			}()

			if got := run(); got != tt.want {
				t.Errorf("run() = %v, want %v", got, tt.want)
			}

			ow.Close()
			ew.Close()
			os.Stdout = oldOut
			os.Stderr = oldErr
			out := <-outC
			err := <-errC

			if out != tt.wantOut {
				t.Errorf("run() stdout '%s', want '%s'", out, tt.wantOut)
			}
			if err != tt.wantErr {
				t.Errorf("run() stderr '%v', want '%v'", []byte(err), []byte(tt.wantErr))
			}
		})
	}
}

// Reset flag module for the tests
func ResetFlagForTesting(usage func()) {
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	flag.CommandLine.Usage = func() { flag.Usage() }
	flag.Usage = usage
}
