package main

import (
	"os/user"
	"strconv"

	"github.com/rs/zerolog/log"
	"github.com/songgao/water"
)

func getOwner(owner string) (uint, uint, error) {
	usr, err := user.Lookup(owner)
	if err != nil {
		usr, err = user.Current()
		if err != nil {
			return 0, 0, err
		}
	}
	uid, err := strconv.Atoi(usr.Uid)
	if err != nil {
		return 0, 0, err
	}
	gid, err := strconv.Atoi(usr.Gid)
	if err != nil {
		return 0, 0, err
	}
	return uint(uid), uint(gid), nil
}

func updateTapConfig(config *water.Config) {
	config.Name = iface
	log.Info().Msgf("TAP     : %s", iface)

	uid, gid, err := getOwner(owner)
	if err != nil {
		log.Warn().Msgf("Cannot use specified user: %v", err)
	}
	config.Permissions = &water.DevicePermissions{
		Owner: uint(uid),
		Group: uint(gid),
	}
	log.Info().Msgf("TAP User: %d (%d)", uid, gid)

	config.Persist = true
}
